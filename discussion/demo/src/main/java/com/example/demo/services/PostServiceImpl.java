package com.example.demo.services;

import com.example.demo.models.Post;
import com.example.demo.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService{

    @Autowired

    private PostRepository postRepository;

    public void createPost (Post post){
        postRepository.save(post);
    }

    public Iterable<Post> getPost(){
        return postRepository.findAll();
    }


    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post Deleted Successfully", HttpStatus.OK);
    }

    public ResponseEntity updatePost(Long id, Post post){
        Post postForUpdating = postRepository.findById(id).get();

        postForUpdating.setTitle(post.getTitle());
        postForUpdating.setContent(post.getContent());
        postRepository.save(postForUpdating);

        return new ResponseEntity<>("Post Updated Successfully", HttpStatus.OK);
    }
}
