package com.example.demo.services;

import com.example.demo.models.Post;
import org.springframework.http.ResponseEntity;


public interface PostService {

    void createPost(Post post);

    Iterable<Post> getPost();

    ResponseEntity deletePost(Long id);
    
    ResponseEntity updatePost(Long id, Post post);
}
