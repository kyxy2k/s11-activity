package com.example.demo.services;

import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl {

    @Autowired
    private UserRepository userRepository;

    // Create user
    public void createUser(User user){
        userRepository.save(user);
    }

    // Get users
    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    // Update user
    public ResponseEntity updateUser(Long id, User user){
        User userForUpdating = userRepository.findById(id).get();

        userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);

        return new ResponseEntity<>("User updated successfully!", HttpStatus.OK);
    }

    // Delete user
    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully!", HttpStatus.OK);
    }

}
