package com.example.demo.controllers;

import com.example.demo.models.Post;
import com.example.demo.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {


    @Autowired
    PostService postService;

   @PostMapping("/posts")

    public ResponseEntity<Object> createPost(@RequestBody Post post){
        postService.createPost(post);
        return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost(){
       return new ResponseEntity<>(postService.getPost(), HttpStatus.OK);
    }

    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid){
       return postService.deletePost(postid);
    }

    @RequestMapping(value = "/posts/{postid}" , method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestBody Post post){
       return postService.updatePost(postid,post);
    }
}
