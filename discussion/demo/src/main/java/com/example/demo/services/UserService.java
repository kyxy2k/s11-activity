package com.example.demo.services;

import com.example.demo.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    void createUser(User user);

    // Retrieve
    Iterable<User> getUsers();

    // Update
    ResponseEntity updateUser(Long id, User user);

    // Delete
    ResponseEntity deleteUser(Long id);

}
